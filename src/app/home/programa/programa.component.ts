import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-programa',
  templateUrl: './programa.component.html',
  styleUrls: ['./programa.component.css']
})
export class ProgramaComponent implements OnInit {
  @Input() nombre:string = "XPROGRAM"
  @Input() icono:string = "assets/img/svg/089.svg"
  constructor() { }

  ngOnInit(): void {
  }

}
