import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //Variable especial, remover en producción y agregar codigo correspondiente a login()
  private isLogin:boolean=true
  
  
  constructor(private router: Router) { }

  login():boolean|void{
    if(this.isLogin==true){
      this.router.navigate(["home"])
    }else{
      return false;
    }
  }


}
