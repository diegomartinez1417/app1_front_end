import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MissingComponent } from '../popups/missing/missing.component';
import { LoginComponent } from './login.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children:[

    ]
  },
  {
    path:'missing',
    component: MissingComponent,
    outlet: 'popup'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
