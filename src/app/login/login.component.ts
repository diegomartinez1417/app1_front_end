import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { izquierdaADerechaFade } from '../animations';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [izquierdaADerechaFade]
})
export class LoginComponent implements OnInit {

  isVisible:boolean = false;
  isDenegado:boolean = false;
  formularioAcceso:FormGroup;

  constructor(
    private router:Router,
    private route:ActivatedRoute,
    private _fb:FormBuilder,
    private _loginService:LoginService
    ) { 
    this.formularioAcceso = this._fb.group({
      'usuario':['nombre',Validators.required],
      'password':['',Validators.compose([Validators.required,Validators.minLength(8),Validators.maxLength(16)])]
    })
    }

  ngOnInit(): void {          
  }

  login(){
    let resultado = this._loginService.login()
    resultado == false?this.isDenegado=true:this.isDenegado=false;
  }


  thechanges(){
    console.log(this.formularioAcceso.controls)
  }
  

}
