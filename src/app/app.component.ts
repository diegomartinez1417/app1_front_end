import { animate, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { animacionInicio } from './animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('routerStartEnd',[
    transition('void=>*',[
        style({
            backgroundColor:'red'
        }),
        animate('50s easeIn',style({
            backgroundColor:"green"
        }))
    ])
])]
})
export class AppComponent {
}
