import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MissingComponent } from './popups/missing/missing.component';
import { MatIconModule } from '@angular/material/icon';

const routes:Routes =  [
  {
    path:'login',
    loadChildren: ()=>import('./login/login.module').then(m=>m.LoginModule),
    title: "Iniciar sesión"
  },
  {
    path:'home',
    loadChildren: ()=>import('./home/home.module').then(m=>m.HomeModule)
  },
  {
    path:'missing',
    component: MissingComponent,
    outlet: 'popup'
  },
  {
    path:'**',
    redirectTo: 'login',
    pathMatch: 'full'
  }
]

@NgModule({
  declarations: [
    AppComponent, MissingComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
